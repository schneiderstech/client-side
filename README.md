# README #

Server side App using Symfony.

### Steps to get Angular CLI up and running ###
To complete the App, you need Node.js and Angular CLI installed in your computer.

* To install Node.js:
[Download](https://nodejs.org/en/) the version for your system and choose the default options to complete the installation.
Run node -v from your OS command line to verify the version number.

* To install Angular CLI
```npm install -g angular-cli```

```npm new client-side```

```cd client-side```

```ng serve```


This project was generated with [angular-cli](https://github.com/angular/angular-cli) version 1.0.0-beta.26.

## Development server
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive/pipe/service/class/module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Further help

To get more help on the `angular-cli` use `ng help` or go check out the [Angular-CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
