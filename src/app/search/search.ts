export class Search {
    key: string;
    airline: Array<{
        code: string,
        name: string
    }>;
    flightNum: number;
    start: Array<{
        dateTime: Date;
        airportCode: string,
        airportName: string,
        cityCode: string,
        cityName: string,
        countryCode: string,
        countryName: string,
        latitude: number,
        longitude: number,
        stateCode: string,
        timeZone: string       
    }>;
    finish: Array<{
        dateTime: Date;
        airportCode: string,
        airportName: string,
        cityCode: string,
        cityName: string,
        countryCode: string,
        countryName: string,
        latitude: number,
        longitude: number,
        stateCode: string,
        timeZone: string       
    }>;
    plane: Array<{
        code: number,
        shortName: string,
        fullName: string,
        manufacturer: string,
        model: string
    }>;
    distance: number;
    durationMin: number;
    price: number;

}

