import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';

import { SearchService } from './search.service';
import { Airports } from '../../providers/airports';
import { Search } from './search';

@Component({
    selector: 'app-search',
    templateUrl: './search.component.html',
    styleUrls: ['./search.component.css'],
    providers: [FormBuilder, SearchService]
})
export class SearchComponent implements OnInit {

    formSearch: FormGroup;
    
    fromLocation: string;
    toLocation: string;
    dateSelected: any;
    dateRange: number;

    airports: Airports[];
    airportByCode: Airports[];
    search: Search[];

    constructor(
        private searchService: SearchService,
        private formBuilder: FormBuilder) { 

            this.formSearch = formBuilder.group({
                fromLocation: ['', Validators.compose([Validators.required])],
                toLocation: ['', Validators.compose([Validators.required])],
                dateSelected: ['', Validators.compose([Validators.required])],
                dateRange: ['', Validators.compose([Validators.required])]
            })
    }

    ngOnInit() {
        this.searchService.getAirports()
                          .subscribe((data) => this.airports = data);
    }
    
    submitSearch(fromLocation: string, toLocation: string, dateSelected: Date, dateRange: number) {

        if(this.formSearch.valid){
            this.searchService.getSearch(this.formSearch)
                .subscribe((data) =>this.search = data);
        } 
    }

}
