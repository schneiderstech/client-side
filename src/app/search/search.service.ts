import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

@Injectable()
export class SearchService {

    constructor(private http: Http) { }


    getAirports(){
        let airportsUrl = 'http://localhost:8000/airports';

        return this.http.get(airportsUrl)
                        .map((res) => res.json())
                        .catch(this.handleError)
    }

    getSearch(formSearch){

        let searchsUrl = 'http://localhost:8000/flight_search/date=' + formSearch.value.dateSelected + 
                        '&from=' + formSearch.value.fromLocation + 
                        '&to=' + formSearch.value.toLocation +
                        '&range=' + formSearch.value.dateRange;

        return this.http.get(searchsUrl)
                        .map((res) => res.json())
                        .catch(this.handleError)
    }

    private handleError (error: Response | any) {
        let errMsg: string;
        
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable.throw(errMsg);
    }

}
